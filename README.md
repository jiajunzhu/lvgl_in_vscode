# lvgl_in_vscode

一）介绍

windows系统，vscode安装PC模拟器运行lvgl。（ps：既然vscode可以，那么像Clion等IDE就都可以。）

二）内容

基于官方eclipse模拟器项目。

三）问题

搭建的过程中遇到很多问题，也参考了几个博主的教程，最终实现vscode中运行lvgl。
之后会考虑出一个自己搭建的过程及一些问题的解决。

四）声明

当然，本人还有很多东西要学，欢迎小伙伴们的讨论。
